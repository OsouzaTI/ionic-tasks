import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { AddTaskInputComponent } from './components/add-task-input/add-task-input.component';



@NgModule({
  declarations: [AddTaskInputComponent],
  imports: [CommonModule, IonicModule.forRoot(),],
  exports: [AddTaskInputComponent]
})
export class TasksModule { }
